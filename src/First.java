import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.WindowConstants;

import com.csvreader.CsvReader;
import com.healthmarketscience.jackcess.ColumnBuilder;
import com.healthmarketscience.jackcess.DataType;
import com.healthmarketscience.jackcess.Database;
import com.healthmarketscience.jackcess.Database.FileFormat;
import com.healthmarketscience.jackcess.DatabaseBuilder;
import com.healthmarketscience.jackcess.Table;
import com.healthmarketscience.jackcess.TableBuilder;


public class First {
	
	static ArrayList<ColumnObject> mDBColumns;
	
	static String[] columnNames = new String[]{"ADV_SRLNO", "ADV_DATE", "BOK_PLACE", "ADV_AMT", "BOK_FROM", "BOK_UPTO", "SRLNO", "CNAME", "CADD1", "CADD2", "REMARK1",
			"REMARK2", "EXP_NO", "DESCRIPTION", "ROOM_NOS", "ID_PROOF", "ID_NO", "AGE", "OCUPATION", "STATE", "PHONE", "NOPER", "MALE", "FEMALE", "CHILD", "CRELA", 
			"NATLY", "MOB_NO", "MAIL_ID", "DOB", "DOA", "VISIT"};
	
	static String[] mapNames = new String[]{"Booking ID", "Reservation Date", "", "Net Amount", "Expected Check-In", "Expected Check-Out", "", "Guest Name", "Address",
		"", "", "", "", "", "", "Id Type", "Id No", "Age", "", "", "Mobile Number", "", "", "", "", "", "", "", "Email Id", "DOB", "DOA", ""};
	
	static String[] reqColumnNames = {"Booking ID", "Room Type", "Guest Name", "Mobile Number", "Email Id", "Address", "Reservation Date", 
	"Expected Check-In", "Expected Check-Out", "Booking Source", "Net Amount", "Booking Status", "Age", "DOB", "DOA", "Id Type", "Id No"};
	
//	String[] mapNames = {"ADV_SRLNO", "", "CNAME", "PHONE", "MAIL_ID", "CADD1", "ADV_DATE", "BOK_FROM", "BOK_UPTO", "", "ADV_AMT", "", 
//	"AGE", "DOB", "DOA", "ID_PROOF", "ID_NO"};
	
	static String CSVPATH, MDBPATH;
	
	public static void main(String[] args) throws IOException {
		getColumnDetails();
		
		JFrame f = new JFrame();// creating instance of JFrame
		f.setSize(500, 600);
		f.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

		JButton b = new JButton("click");// creating instance of JButton
		b.setBounds(200, 280, 100, 40);// x axis, y axis, width, height
		f.add(b);// adding button in JFrame
		
		JTextArea text = new JTextArea("\u00a9 Appstone Private Limited");
		text.setBounds(150,500, 200,20);
		text.setAlignmentX(JTextArea.CENTER_ALIGNMENT);
		f.add(text);
		
		f.setLayout(null);// using no layout managers
		f.setVisible(true);// making the frame visible
		
		b.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				JFileChooser chooser = new JFileChooser();
				chooser.setCurrentDirectory(new java.io.File("."));
				chooser.setDialogTitle("File Choosing");
				chooser.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
				chooser.setAcceptAllFileFilterUsed(false);
				if (chooser.showOpenDialog(chooser) == JFileChooser.APPROVE_OPTION) {
					System.out.println("CSV getCurrentDirectory(): " + chooser.getCurrentDirectory());
					System.out.println("CSV getSelectedFile() : "+ chooser.getSelectedFile());
					CSVPATH = chooser.getSelectedFile().toString();
					
					JFrame nameFrame = new JFrame("Enter File Name");
					nameFrame.setSize(200, 300);
					nameFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
					
					JTextField fileName = new JTextField();
					fileName.setBounds(25, 130, 150, 40);
					nameFrame.add(fileName);
					
					JButton btnFile = new JButton("OK");
					btnFile.setBounds(75, 190, 50, 20);
					nameFrame.add(btnFile);
					
					btnFile.addActionListener(new ActionListener() {
						
						@Override
						public void actionPerformed(ActionEvent e) {
							String FileName = fileName.getText().toString();
							System.out.println("FileName: "+ FileName);
							JFileChooser mdbFileChooser = new JFileChooser();
							mdbFileChooser.setCurrentDirectory(new java.io.File("."));
							mdbFileChooser.setDialogTitle("Save MDB File To");
							mdbFileChooser.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
							mdbFileChooser.setAcceptAllFileFilterUsed(false);
							if(mdbFileChooser.showOpenDialog(mdbFileChooser) == JFileChooser.APPROVE_OPTION){
								System.out.println("MDB getCurrentDirectory(): " + mdbFileChooser.getSelectedFile());
								MDBPATH = mdbFileChooser.getSelectedFile().toString();
								MDBPATH = MDBPATH + "/" + FileName + ".mdb";
								try {
									convertCSVtoMDB(CSVPATH, MDBPATH);
								} catch (IOException e1) {
									// TODO Auto-generated catch block
									e1.printStackTrace();
								}
								nameFrame.setVisible(false);		
							}else{
								
							}
						}
					});
					
					nameFrame.setLayout(null);
					nameFrame.setVisible(true);
					
					
				} else {
					System.out.println("No Selection ");
				}
			}
		});
		
	}
	
	/*
	 * This method gets all the column names in a array to validate while MDB Conversion.
	 */
	private  static void getColumnDetails(){
		mDBColumns = new ArrayList<ColumnObject>();
		if(columnNames.length == mapNames.length){
			for (int a=0; a< columnNames.length; a++){
				ColumnObject object = new ColumnObject();
				object.setCOLUMN_NAME(columnNames[a]);
				object.setMATCH_NAME(mapNames[a]);
				mDBColumns.add(object);
			}
		}
	}

	private static void convertCSVtoMDB(String filePath, String directory) throws IOException{
		 try {
		        Database database = createDatabase(directory);
		        ArrayList<String> Header_Values = new ArrayList<String>();
		        CsvReader products = new CsvReader(filePath);
		        products.readHeaders();
		        String Header = "";
		        Header = products.getRawRecord();
		        String[] Header_Split = Header.split(",");
		        for (int i = 0; i < Header_Split.length; i++) {
		            Header_Values.add(Header_Split[i]);
		        }

		        Table table;
		        TableBuilder tb = createTable("ADV1");
		        
		        for (int t =0; t < columnNames.length; t++){
		        	tb.addColumn(new ColumnBuilder(columnNames[t]).setType(DataType.TEXT).toColumn());
		        }


		        table = tb.toTable(database);
		        int Column_Size = columnNames.length;
		        while (products.readRecord()) {
		            String[] name = new String[Column_Size];
		            
		            for (int x=0; x<Column_Size; x++){
		            	ColumnObject obj = mDBColumns.get(x);
		            	if(obj.getMATCH_NAME().isEmpty()){
		            		name[x] = "";
		            	}else{
		            		int pos = returnPositionValue(obj);
		            		name[x] = products.get(pos).toString();
		            	}
		            }
//		            int Count = table.getRowCount();
//		            System.out.println(Count);
		            try {
		                table.addRow(name);
		            } catch (Exception ex) {
		                ex.printStackTrace();
		            }
		            name = null;
			}
			products.close();
			System.out.println("**Completed**");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private static int returnPositionValue(ColumnObject obj) {
		int pos = 0;

		for (int m = 0; m < reqColumnNames.length; m++) {
			String columnName = reqColumnNames[m];
			if (columnName.equalsIgnoreCase(obj.getMATCH_NAME())) {
				pos = m;
			}
		}

		return pos;
	}

	private static Database createDatabase(String databaseName)
			throws IOException {
		return DatabaseBuilder.create(FileFormat.V2010, new File(databaseName));
	}

	private static TableBuilder createTable(String tableName) {
		return new TableBuilder(tableName);
	}
}
