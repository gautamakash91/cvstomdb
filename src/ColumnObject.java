import java.util.ArrayList;


public class ColumnObject {
	
	String COLUMN_NAME;
	String MATCH_NAME;
	
	
	public String getCOLUMN_NAME() {
		return COLUMN_NAME;
	}
	public void setCOLUMN_NAME(String cOLUMN_NAME) {
		COLUMN_NAME = cOLUMN_NAME;
	}
	public String getMATCH_NAME() {
		return MATCH_NAME;
	}
	public void setMATCH_NAME(String mATCH_NAME) {
		MATCH_NAME = mATCH_NAME;
	}
	
	
	
}
